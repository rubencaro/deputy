# Deputy (Dependencies Utility)

It allows to visualize external dependencies of given module.

Usage example: `depupy | dot -Tsvg -o deps.svg`

It will print DOT code representing the external dependencies for current module (i.e. not std library).
It is only based on `go mod graph` and `go list -m all` outputs on current folder.
