package main

import (
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func main() {
	generateGraph()
}

func generateGraph() {
	deps := getDeps()
	edges := getEdges(deps)
	dot := getDot(edges)
	fmt.Println(dot)
}

func getEdges(deps []string) [][]string {
	edges := [][]string{}
	graph := run("go", "mod", "graph")
	fromGraph := strings.Split(graph, "\n")
	for _, line := range fromGraph {
		pair := strings.Split(line, " ")
		if contains(deps, pair[0]) && contains(deps, pair[1]) {
			edges = append(edges, pair)
		}
	}
	return edges
}

func getDeps() []string {
	l := run("go", "list", "-m", "all")
	l = strings.ReplaceAll(l, " ", "@")
	return strings.Split(l, "\n")
}

func getDot(edges [][]string) string {
	var dot strings.Builder
	fmt.Fprintf(&dot, `digraph dependencies {
rankdir=LR
node[shape=box style=rounded height=0 width=0]
`)
	printEdges(&dot, edges)
	fmt.Fprintf(&dot, "}\n")
	return dot.String()
}

func run(cmd string, args ...string) string {
	out, err := exec.Command(cmd, args...).Output()
	if err != nil {
		log.Fatal(err)
	}
	return strings.TrimRight(string(out), "\n")
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func printEdges(b *strings.Builder, edges [][]string) {
	for _, edge := range edges {
		fmt.Fprintf(b, "%q -> %q\n", edge[0], edge[1])
	}
}
